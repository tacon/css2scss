package main

import (
	"bytes"
	"strings"

	"github.com/aymerick/douceur/css"
	cssparse "github.com/aymerick/douceur/parser"
)

func css2scss(text string) (string, error) {
	stylesheet, err := cssparse.Parse(text)
	if err != nil {
		return "", err
	}

	node := treeFrom(stylesheet)

	var buf = bytes.NewBuffer(nil)
	node.format(buf, "")

	return buf.String(), nil
}

func treeFrom(s *css.Stylesheet) *selectorNode {
	root := &selectorNode{}

	for _, rule := range s.Rules {
		for _, selector := range rule.Selectors {
			root.add(rule, strings.Split(selector, " "))
		}
	}

	return root
}

func (s *selectorNode) add(rule *css.Rule, pieces []string) {
	for _, child := range s.children {
		if child.val == pieces[0] {
			if len(pieces) == 1 {
				s.rules = append(s.rules, rule)
			} else {
				child.add(rule, pieces[1:])
			}
			return
		}
	}

	// no match was found
	node := &selectorNode{
		val: pieces[0],
	}
	s.children = append(s.children, node)

	if len(pieces) > 1 {
		node.add(rule, pieces[1:])
	} else if len(pieces) == 1 {
		node.rules = append(node.rules, rule)
	}
}

type selectorNode struct {
	val   string
	rules []*css.Rule

	children []*selectorNode
}

func (s *selectorNode) format(buf *bytes.Buffer, indent string) {
	if len(s.val) != 0 {
		buf.WriteString(indent)
		buf.WriteString(s.val)
		buf.Write(openBraceBytes)
	}

	currIndent := indent + "  "
	if len(s.val) == 0 {
		currIndent = indent
	}

	for _, rule := range s.rules {
		for _, decl := range rule.Declarations {
			buf.WriteString(currIndent)
			buf.WriteString(decl.String())
			buf.Write(endlineBytes)
		}
	}

	for i, child := range s.children {
		child.format(buf, currIndent)
		if i != len(s.children)-1 {
			buf.Write(endlineBytes)
		}
	}

	if len(s.val) != 0 {
		buf.WriteString(indent)
		buf.Write(closeBraceBytes)
	}
}

var (
	openBraceBytes  = []byte(" {\n")
	closeBraceBytes = []byte("}\n")
	endlineBytes    = []byte("\n")
)
