package main

import "testing"

func Test_css2scss(t *testing.T) {
	fmtted, err := css2scss(`
#action li {
  color: #000;
}

#action span {
  color: #fff;
}
`)
	expected := `#action {
  li {
    color: #000;
  }

  span {
    color: #fff;
  }
}
`

	if err != nil {
		t.Errorf("failed to transform css: ", err)
		t.Fail()
	} else if expected != fmtted {
		t.Errorf("did not get correctly transformed css")
	}
}
